# exercise_timeseries


## Description

A project to plot the temperature timeseries .csv and .nc files. 

If disered a linear trend can be added to the plot. The data of the plots
can be saved to a directory if given. 

## Installation

You need a working Python environment, and `pip` installed.

E.g., with `conda`:

```bash
conda create --name mynewenv python
conda activate mynewenv
```

Use the following command in the base directory to install the project:

```bash
python -m pip install .
```

## Usage

```bash
plot_timeseries --data-file <filename> --anomalies --trend --output-dir <output-dir> 
```

For usage information, type `plot_timeseries --help`.


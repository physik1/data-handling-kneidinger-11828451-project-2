import matplotlib.pyplot as plt
import pandas as pd
from pathlib import Path
import matplotlib.dates as mdates


def plot_timeseries(ds, title, start_date="", end_date="", output_dir="", trend=""):
    """Plots time series of temperature data.

    Args:

        ds (xarray dataset): Data to be plotted. Must contain at
            least the parameter "t".
        title: Contains the title of the figure

        start_date (datetime.date): Input start date of the plotted ds

        end_date (datetime.date): Input end date of the plotted ds

        output_dir (string, default: ""): If string is not empty, save
            figure to this directory. If empty, just show the figure.

        trend: If given a linear trend is added to the plot
    """

    print("plot data")
    nr_years = end_date.year - start_date.year

    fig, ax = plt.subplots()
    ax.plot(ds.time, ds.t)
    if nr_years <= 2:
        plt.gca().xaxis.set_major_formatter(mdates.DateFormatter("%Y-%m"))
    else:
        plt.gca().xaxis.set_major_formatter(mdates.DateFormatter("%Y"))
    plt.gca().xaxis.set_major_locator(mdates.AutoDateLocator())

    if trend:
        #Error analysis still missing :(
        ax.plot(ds.time, ds["trend"], label="linear trend")

    ax.set(title=title)
    plt.legend()
    ax.set_ylabel("t (K)")
    plt.show()

    if output_dir:
        timestamp = pd.Timestamp.now().strftime("%Y-%m-%dT%H:%M:%S")
        fn = Path(output_dir).joinpath(f"{timestamp}-timeseries.png")
        print(f"Save plot to: {fn}")
        plt.savefig(fn, dpi=200, bbox_inches="tight")
    else:
        plt.show()

import pandas as pd
import xarray as xr
import pathlib


def read_data(fn, start_date, end_date, latitude_range):
    """Reads data from file.

    Args:
        fn: Filename/path of file containing the data.
        start_date: selects the start (datetime object YYYY-MM-DD) of the timeseries input data
        end_date: selects the end (datetime object YYYY-MM-DD) of the timeseries input data.

    Returns: xarray dataset containing the data between start_date and end_date
    """
    print("Check filetype")

    # Check the extension of the data file:
    file_type = pathlib.Path(fn).suffix

    if file_type == ".nc":
        print("Read netCDF file")
        with xr.open_dataset(fn) as ds:
            ds.load()
        ds = ds.rename_vars({"temperature_2_meter": "t"})
        ds = ds.sel(latitude=slice(*latitude_range)).mean(["longitude", "latitude"])

    if file_type == ".csv":
        print("Read csv file")
        df = pd.read_csv(fn, parse_dates=["time"], index_col="time")
        ds = pd.DataFrame.to_xarray(df)

        # SI Units °C to K
        ds["t"].data = ds["t"].data + 273.15
        ds["t"].attrs["units"] = "K"

    return ds


def write_data(data, output_dir=""):
    print("Write data to netCDF file")
    data_nc = data.to_netcdf(output_dir)
    return data_nc

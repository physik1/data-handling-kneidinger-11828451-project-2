import datetime

import pandas as pd
import numpy as np


def anomalies(ds, start_date, end_date):
    """Compute monthly anomalies for the given data, relative to some reference time.

    Args:

        ds (xarray dataset): Data for which the monthly anomalies
            are computed. The dataset must contain a valid datetime
            index.
        start_date (datetime.date): Selected start date in ISO-format
        end_date (datetime.date): Selected end date in ISO-format


    Returns: pandas dataframe containing timeseries of monthly anomalies.

    """

    print("Compute anomalies.")

    clim_ref_period = (
        datetime.date.fromisoformat("1991-01-01"),
        datetime.date.fromisoformat("2020-12-31"),
    )

    clim = ds.sel(time=slice(*clim_ref_period)).groupby("time.month").mean()

    if start_date and end_date:
        ds = ds.sel(time=slice(start_date, end_date))

    if start_date and not end_date:
        print("start")
    if end_date and not start_date:
        print("end")

    anom = ds.groupby("time.month") - clim

    return anom


def linear_trend(ds, start_date, end_date):
    """Computes a linear trend for given data.

    Args:
        ds(xarray dataset):  The dataset must contain a valid datetime
            index.
        start_date (datetime.date): Input start date of the plotted ds

        end_date (datetime.date): Input end date of the plotted ds

    Returns: an xarray dataset containing the data and values ("trend" )to plot a linear trend
    """

    if start_date and end_date:
        ds = ds.sel(time=slice(start_date, end_date))

    if start_date and not end_date:
        print("I was not able to fix this yet")
    if end_date and not start_date:
        print("I was not able to fix this yet")

    time = np.arange(ds["t"].dropna("time").shape[0])
    temp = ds["t"].dropna("time").values

    coef = np.polyfit(time, temp, deg=1)
    time_fit = np.arange(ds["t"].shape[0])
    temp_fit_func = np.poly1d(coef)
    temp_fit = temp_fit_func(time_fit)

    ds = ds.assign({"trend": ds["t"] * 0 + temp_fit})

    return ds
